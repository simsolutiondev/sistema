<?php
class Projeto extends CI_Controller {
    public function __construct(){
		parent::__construct();
		$this->load->helper('url');
    }
    
    public function projetos() {
        $crud = new grocery_CRUD();
//        
        $crud->set_table('projetos');
        $crud->set_subject("Projetos");
        
        $crud->fields('codCliente','codFuncionario','descricao','dataContrato','dataConclusao','status'); //campos para inclusao
        
        $crud->required_fields('codCliente','codFuncionario','descricao','dataContrato','dataConclusao','status');
        
        $crud->display_as("codCliente", "Cliente");
        $crud->set_relation("codCliente", "clientes", "nome");
        $crud->display_as("codFuncionario", "Funcionário");
        $crud->set_relation("codFuncionario", "funcionarios", "nome");
        $crud->display_as("valor", "Valor");
        $crud->display_as("dataContrato", "Data de Início");
        $crud->display_as("dataConclusao", "Data de Fim");   
        $crud->display_as("status", "Status");
        $crud->set_relation("status", "status", "descricao");

        
        $crud->unset_clone();
        
        $output = $crud->render();    
        
        $this->template->load("layout/painel", "cruds/viewCruds", (array) $output);
        
    }
    
    public function itensProjeto() {
        $crud = new grocery_CRUD();
        
        $crud->set_table('projetosItens');
        $crud->set_subject("Itens Projetos");
        
        $crud->fields('codProjeto','codProduto','qtd'); //campos para inclusao
        
        $crud->display_as("codProjeto", "Codigo Projeto");
        $crud->set_relation("codProjeto", "projetos", "codigo");
        $crud->display_as("codProduto", "Codigo Produto");
        $crud->set_relation("codProduto", "produtos", "descricao");
        $crud->display_as("qtd", "Quantidade");
        $crud->display_as("valortotal", "Valor Total");
        $crud->display_as("valorItem", "Valor Item");
        
        $crud->unset_clone();
        $output = $crud->render(); 
        $this->template->load("layout/painel", "cruds/viewCruds", (array) $output);
       
    }
        
  
}