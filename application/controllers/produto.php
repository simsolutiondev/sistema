<?php
class Produto extends CI_Controller {
    public function __construct(){
		parent::__construct();
		$this->load->helper('url');
    }
    
    public function produtos() {
        $crud = new grocery_CRUD();
//        
        $crud->set_table('produtos');
        $crud->set_subject("Produtos");
        
        $crud->display_as("descricao", "Descricao");
        $crud->display_as("dataCadastro", "Data Cadastro");
        $crud->display_as("ativo", "Ativo");
        $crud->display_as("valorCompra", "Valor Compra");
        $crud->display_as("valorVenda", "Valor Venda");
        
        $lista_status = array('1' => 'Ativo', '0' => 'Inativo');
        $crud->field_type('ativo', 'dropdown', $lista_status);
        
        $crud->unset_clone();
        
        $output = $crud->render();    
        
        $this->template->load("layout/painel", "cruds/viewCruds", (array) $output);
        
    }
        
  
}