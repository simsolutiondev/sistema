<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class login extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $dados= array();
        $this->template->load("usuario/viewLogin","usuario/viewLogin", $dados);
    }

    public function acessar() {
        if ($this->input->method() == "post") {
            $this->load->model("usuarios"); // chama o modelo usuarios_model
            $login = $this->input->post("login"); // pega via post o email que vem do formulario
            $senha = $this->input->post("senha"); // pega via post a senha que vem do formulario

            $usuario = $this->usuarios->validarUsuario($login, $senha); // acessa a função buscaPorEmailSenha do modelo

            if ($usuario) {
                $this->session->set_userdata("usuario", $usuario);
                $dados = array("mensagem" => "Logado com sucesso!");
                ?><script>
                    window.location.replace("http://localhost/marcenaria/usuario/usuarios");
                    //alert("Logado com sucesso");
                  </script> 
                <?php
            } else {
                ?><script>
                    window.location.replace("http://localhost/marcenaria/login/acessar");
                    alert("Usuário ou senha inválidos");
                  </script> 
                <?php
            }
        } else {
            $dados = array();
        }
        $this->template->load("usuario/viewLogin","usuario/viewLogin", $dados);
    }
    
    public function sair() {
        $this->session->set_userdata("usuario", null);
        redirect("/login/");
    }

}
