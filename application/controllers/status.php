<?php
class Status extends CI_Controller {
    public function __construct(){
		parent::__construct();
		$this->load->helper('url');
    }
    
    public function status() {
        $crud = new grocery_CRUD();
//        
        $crud->set_table('status');
        $crud->set_subject("Status");
        
        $crud->display_as("descricao", "Descrição do Status");
        $crud->required_fields("descricao");
        $crud->unique_fields("descricao");
        
        $crud->unset_clone();
        
        $output = $crud->render();    
        
        $this->template->load("layout/painel", "cruds/viewCruds", (array) $output);
        
    }
        
  
}