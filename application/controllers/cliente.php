<?php
class Cliente extends CI_Controller {
    public function __construct(){
		parent::__construct();
		$this->load->helper('url');
    }
    
    public function clientes() {
        $crud = new grocery_CRUD();
//        
        $crud->set_table('clientes');
        $crud->set_subject("Clientes");
      //  $crud->columns('nome', 'dataCadastro', 'endereco', 'numero', 'codCidade', 'telefone', 'celular', 'email');
       // $crud->fields('codcategoria','nome','status');
        
        $crud->display_as("nome", "Nome");
        $crud->display_as("dataCadastro", "Data Cadastro");
        $crud->display_as("endereco", "Endereço");
        $crud->display_as("codCidade", "Cidade");
        $crud->display_as("codEstado", "UF");   
        $crud->display_as("codPais", "Pais");
        $crud->display_as("celular", "Celular");
        $crud->display_as("email", "E-mail");
        
        $crud->set_relation("codCidade", "municipios", "nome");
        $crud->set_relation("codEstado", "estados", "uf");
        $crud->set_relation("codPais", "paises", "nome");
        
        $lista_status = array('1' => 'Ativo', '0' => 'Inativo');
        $crud->field_type('ativo', 'dropdown', $lista_status);
        
        $crud->unset_clone();
        
        $output = $crud->render();    
        
        //$this->load->view("layout/teste", $output);
        $this->template->load("layout/painel", "cruds/viewCruds", (array) $output);
        
    }
        
  
}
