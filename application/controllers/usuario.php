<?php
class Usuario extends CI_Controller {
    public function __construct(){
		parent::__construct();
		$this->load->helper('url');
    }
    
    public function usuarios() {
        $crud = new grocery_CRUD();
//        
        $crud->set_table('usuarios');
        $crud->set_subject("Usuarios");
        
//        $crud->fields("usuario","senha","confirma", "dataCadastro","codFuncionario","ativo"); //atribuindo quais campos devem aparecer no cadastro
        
        $crud->display_as("usuario", "Usuário");
        $crud->display_as("senha", "Senha");
//        $crud->display_as("confirma", "Confirma Senha");
        $crud->display_as("dataCadastro", "Data de Cadastro");
        $crud->display_as("codFuncionario", "Funcionário Vinculado");
        $crud->display_as("ativo", "Ativo (S/N)");
        $lista_status = array('1' => 'Ativo', '0' => 'Inativo');
        $crud->field_type('ativo', 'dropdown', $lista_status);
        
        $crud->callback_before_insert(array($this, "remove_confirma")); //removendo confirma da lista de salvar no momento de confirmar o formulário
        
        $crud->required_fields("usuario","senha","confirma","ativo");
        $crud->unique_fields("usuario");
        $crud->field_type('senha', 'password');        
//        $crud->field_type('confirma', 'password');
        
        //$crud->edit_fields("senha", "confirma", "ativo"); // controla quais campos aparecem na ediçao
        //$crud->add_fields(); // para controlar quais campos aparecem no adicionar
        
//        $crud->set_rules("confirma", "Confirma Senha", "matches[senha]|min_length[5]"); // efetuando comparação de senhas informadas
        $crud->set_rules("senha", "Senha", "min_length[5]"); //colocando numero minimo de carcteres pra senha

        
        $crud->unset_clone();
        
        $output = $crud->render();    
        
        $this->template->load("layout/painel", "cruds/viewCruds", (array) $output);
        
    }
    
    public function remove_confirma($post_array) {
//        unset($post_array['confirma']);
        $post_array['senha'] = md5($post_array['senha']);
        return $post_array;
    }
        
  
}