<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Usuários
    </h1>
  
</section>
<?php foreach ($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach ($js_files as $file): ?>
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>     
<!--conteúdo da página-->
<div class="container-fluid">
    
    <div class="row">
        <?php echo $output; ?>
    </div>
    <hr/>
</div> <!-- /container -->
<!--fim do conteúdo da página-->    


<hr/>